<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'ApiUser',
            'email' => 'apiuser@datanerds.com',
            'password' => bcrypt(str_random(10)),
            'api_token' => str_random(55)
        ]);
    }
}
