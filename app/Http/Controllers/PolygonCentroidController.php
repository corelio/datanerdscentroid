<?php

namespace App\Http\Controllers;

use App\Traits\Centroid;
use Illuminate\Http\Request;

class PolygonCentroidController extends Controller
{

    use Centroid;

    public function findCentroid(Request $request){
        if(!$request['polygon'])
            return response()->json(['Error: no polygon found']);

        return $this->calculate($request['polygon']);
    }
}
