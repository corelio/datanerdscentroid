<?php

namespace App\Traits;

trait Centroid
{

    protected $polygon = [];

    protected $calculatedArea = 0;

    public function calculate($polygon){
        $this->polygon = $polygon;
        //Check if polygon is not closed and close it if necessary
        $this->closePolygon();
        //calculate the area
        $this->area();
        //calculate the centroid and return the value
        return $this->centroid();

    }

    public function closePolygon(){
        if($this->polygon[count($this->polygon)-1] != $this->polygon[0])
        {
            $this->polygon[count($this->polygon)] = $this->polygon[0];
        }
    }

    public function area() {
        $area = 0;
        for($i=0; $i<count($this->polygon)-1; $i++){
            $ii = ($i + 1) % count($this->polygon);
            $area += ($this->polygon[$i][1] + $this->polygon[$ii][1]) * ($this->polygon[$ii][0] - $this->polygon[$i][0]);
        }
        $this->calculatedArea = abs($area/2);
    }

    public function centroid() {
        if(!$this->calculatedArea)
            return response()->json(['Error: Area not calculated']);

        $lat = 0;
        $lng = 0;

        for($i=0; $i<count($this->polygon)-1;$i++)
        {
            $f = ($this->polygon[$i][0] * $this->polygon[$i + 1][1]) - ($this->polygon[$i + 1][0] * $this->polygon[$i][1]);
            $lat += ($this->polygon[$i][0] + $this->polygon[$i + 1][0]) * $f;
            $lng += ($this->polygon[$i][1] + $this->polygon[$i + 1][1]) * $f;
        }

        $lat = $lat/6/$this->calculatedArea;
        $lng = $lng/6/$this->calculatedArea;

        return response()->json(['Centroid' => [$lat, $lng]]);
    }
}